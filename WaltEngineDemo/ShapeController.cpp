#include "ShapeController.h"

#include <ctime>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <Components/Renderers/Renderer.h>
#include <Components/Renderers/MeshRenderer.h>
#include <Components/Transform.h>
#include <Physics/Colliders/Collider.h>
#include <Material.h>
#include <Color.h>
#include <GameObject.h>
#include <Input.h>
#include <Material.h>
#include <WaltEngine.h>
#include <PrimitiveGenerator.h>

using namespace demo;
using namespace walt;
using namespace glm;

ShapeController::ShapeController()
{
}

ShapeController::~ShapeController()
{
}

void ShapeController::update()
{
    auto contactPoints = gameObject->collider->localContactPoints();

    for (auto point : contactPoints)
    {
        if (contactPointObject == nullptr)
        {
            contactPointObject = PrimitiveGenerator::createPrimitive(PrimitiveType::Sphere);

            contactPointObject->transform->setParent(transform);
            contactPointObject->transform->scaling = vec3(0.1f);
        }

        //auto point = contactPoints.front();
        contactPointObject->transform->position = point;

        gameObject->renderer->materials.front()->setColor(Color::Red);
    }

    if (contactPoints.size() == 0)
    {
        gameObject->renderer->materials.front()->setColor(vec3(0.8f));
    }
}
