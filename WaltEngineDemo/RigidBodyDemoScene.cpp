#include "RigidBodyDemoScene.h"

#include <ctime>
#include <glm/glm.hpp>
#include <GL/glew.h>

#include <Components/Camera.h>
#include <Components/Renderers/Renderer.h>
#include <Components/Renderers/MeshRenderer.h>
#include <Components/Transform.h>
#include <Physics/Colliders/Collider.h>
#include <BuoyancyController.h>
#include <Color.h>
#include <GameObject.h>
#include <Material.h>
#include <WaltEngine.h>
#include <PrimitiveGenerator.h>

#include <CameraController.h>
#include <ShapeGenerator.h>

using namespace demo;
using namespace walt;
using namespace glm;

RigidBodyDemoScene::RigidBodyDemoScene()
{
    srand((unsigned int)time(NULL));

    // Camera game object
    auto* cameraObject = new GameObject();
    cameraObject->transform->position = vec3(0, 0, 10);

    // Camera
    auto* camera = new Camera();
    cameraObject->addComponent(camera);

    camera->targetPos = vec3(0, 0, 0);
    camera->fieldOfView = 45;
    camera->aspect = (float)WaltEngine::width / WaltEngine::height;
    camera->nearClipPlane = 0.01f;
    camera->farClipPlane = 1000.f;
    camera->setBackgroundColor(Color(49, 77, 121, 5));
    camera->onWindowSizeChanged(WaltEngine::width, WaltEngine::height);

    // Camera controller
    auto* cameraController = new CameraController();
    cameraObject->addComponent(cameraController);

    // Plane
    auto* plane = PrimitiveGenerator::createPrimitive(PrimitiveType::Cube);

    plane->transform->position = vec3(0, 0, 0);
    plane->transform->rotation = quat(vec3(0, 0, 0));
    plane->transform->scaling = vec3(20, 0.01, 20);

    auto* meshRenderer = new MeshRenderer();
    plane->addComponent(meshRenderer);

    auto* material = plane->renderer->getMaterial();
    material->setColor(Color(Color::WaterBlue, 0.5f));
    material->setLightDirection(vec3(1, -1, -1));
    material->setLightColor(vec4(1));
    material->setAmbientIntensity(0.55f);
    material->setDiffuseIntensity(0.6f);
    material->setSpecularIntensity(1);
    material->setSpecularPower(100);

    // Cube
    auto* cube = PrimitiveGenerator::createPrimitive(PrimitiveType::Cube);

    cube->transform->position = vec3(0, 5, 0);
    cube->transform->rotation = quat(Random::randVec3());
    cube->transform->scaling = vec3(1);

    auto* collider = PrimitiveGenerator::createCollider(PrimitiveType::Cube);
    cube->addComponent(collider);
    collider->mass = 0.5f;
    float a = collider->mass();

    auto* script = new BuoyancyController();
    cube->addComponent(script);

    material = cube->renderer->getMaterial();
    material->setColor(vec3(0.4f));
    material->setLightDirection(vec3(1, -1, -1));
    material->setLightColor(vec4(1));
    material->setAmbientIntensity(0.55f);
    material->setDiffuseIntensity(0.6f);
    material->setSpecularIntensity(1);
    material->setSpecularPower(100);
}

RigidBodyDemoScene::~RigidBodyDemoScene()
{
}
