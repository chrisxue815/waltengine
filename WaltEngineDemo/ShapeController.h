#pragma once

#include <list>

#include <Components/Behaviors/MonoBehavior.h>

#include <stdafx.h>

namespace walt
{
    enum class PrimitiveType;
    class GameObject;
}

namespace demo
{
    class ShapeController : public walt::MonoBehavior
    {
    public:
        ShapeController();
        ~ShapeController();

        void update() override;

        walt::GameObject* contactPointObject;
    };
}
