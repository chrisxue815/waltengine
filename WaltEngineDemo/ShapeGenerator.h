#pragma once

#include <list>

#include <Components/Behaviors/MonoBehavior.h>

#include <stdafx.h>

namespace walt
{
    enum class PrimitiveType;
    class GameObject;
}

namespace demo
{
    class ShapeGenerator : public walt::MonoBehavior
    {
    public:
        ShapeGenerator();
        ~ShapeGenerator();

        void update() override;

    protected:
        walt::GameObject* generate(walt::PrimitiveType randType);
        std::list<walt::GameObject*> shapes;
        float cooldown;
        bool aabbRendererEnabled;
    };
}
