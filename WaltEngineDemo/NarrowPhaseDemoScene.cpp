#include "NarrowPhaseDemoScene.h"

#include <glm/glm.hpp>

#include <Components/Camera.h>
#include <Components/Renderers/Renderer.h>
#include <Components/Renderers/MeshRenderer.h>
#include <Components/Transform.h>
#include <Physics/Colliders/Collider.h>
#include <Color.h>
#include <GameObject.h>
#include <Material.h>
#include <WaltEngine.h>
#include <PrimitiveGenerator.h>

#include <CameraController.h>
#include <ShapeGenerator.h>

using namespace demo;
using namespace walt;
using namespace glm;

NarrowPhaseDemoScene::NarrowPhaseDemoScene()
{
    // Camera game object
    auto* cameraObject = new GameObject();
    cameraObject->transform->position = vec3(8, 4, 8);

    // Camera
    auto* camera = new Camera();
    cameraObject->addComponent(camera);

    camera->targetPos = vec3(0, 0, 0);
    camera->fieldOfView = 45;
    camera->aspect = (float)WaltEngine::width / WaltEngine::height;
    camera->nearClipPlane = 0.01f;
    camera->farClipPlane = 1000.f;
    camera->setBackgroundColor(Color(49, 77, 121, 5));
    camera->onWindowSizeChanged(WaltEngine::width, WaltEngine::height);

    // Camera controller
    auto* cameraController = new CameraController();
    cameraObject->addComponent(cameraController);

    // Plane
    auto* plane = PrimitiveGenerator::createPrimitive(PrimitiveType::Cube);

    plane->transform->position = vec3(0, -0.005, 0);
    plane->transform->rotation = quat(vec3(0, 0, 0));
    plane->transform->scaling = vec3(20, 0.01, 20);

    auto* collider = PrimitiveGenerator::createCollider(PrimitiveType::Cube);
    plane->addComponent(collider);
    collider->kinematic = true;

    auto* material = plane->renderer->getMaterial();
    material->setColor(vec3(0.4f));
    material->setLightDirection(vec3(1, -1, -1));
    material->setLightColor(vec4(1));
    material->setAmbientIntensity(0.55f);
    material->setDiffuseIntensity(0.6f);
    material->setSpecularIntensity(1);
    material->setSpecularPower(100);

    // Shape generator
    auto* shapeGeneratorObject = new GameObject();
    auto* script = new ShapeGenerator();
    shapeGeneratorObject->addComponent(script);
}

NarrowPhaseDemoScene::~NarrowPhaseDemoScene()
{
}
