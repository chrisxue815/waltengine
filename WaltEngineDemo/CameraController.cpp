#include "CameraController.h"

#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <Components/Camera.h>
#include <Components/Transform.h>
#include <Input.h>

using namespace demo;
using namespace walt;
using namespace glm;

CameraController::CameraController()
{
}

CameraController::~CameraController()
{/*TODO
    auto* camera = Camera::current->transform;
    camera-mousePos = vec2(x, y);
    const float RotationSpeed = -0.5f;
    const float MovementSpeed = 0.001f;

    float xAngle = (x - prevMousePos.x) * RotationSpeed;
    quat xRotation = angleAxis(xAngle, vec3(0, 1, 0));
    auto offset = xRotation * camera->position;

    vec3 right = cross(vec3(0, 1, 0), offset);
    right = safeNormalize(right);

    float yAngle = (y - prevMousePos.y) * RotationSpeed;
    quat yRotation = angleAxis(yAngle, right);
    offset = yRotation * offset;

    camera->position = offset;*/
}

void demo::CameraController::update()
{
    auto* camera = Camera::current;
    auto cameraPos = camera->transform->position() - camera->targetPos;

    if (Input::curr.mouseDown[GLUT_LEFT_BUTTON] && Input::prev.mouseDown[GLUT_LEFT_BUTTON])
    {
        vec2 offset = Input::getMousePos() - Input::getPrevMousePos();
        if (length(offset) <= 0) return;

        const float rotationSpeed = -0.5f;  // degrees per pixel

        // rotate left-right
        float xAngle = offset.x * rotationSpeed;
        auto up = vec3(0, 1, 0);
        quat xRotation = angleAxis(xAngle, up);
        cameraPos = xRotation * cameraPos;

        // rotate up-down
        float yAngle = offset.y * rotationSpeed;
        
        vec3 right = cross(up, cameraPos);
        right = length(right) > 0 ? normalize(right) : vec3(1, 0, 0);

        vec3 backward = cross(right, up);
        float altitude = acosDegree(dot(cameraPos, backward) / length(cameraPos));
        altitude *= -normalize(cameraPos.y);

        if (altitude + yAngle >= 90) {
            yAngle = 89 - altitude;
        }
        else if (altitude + yAngle <= -90) {
            yAngle = -89 - altitude;
        }

        quat yRotation = angleAxis(yAngle, right);
        cameraPos = yRotation * cameraPos;
    }

    const float ZoomSpeed = -2.0f;

    if (Input::curr.wheel != 0)
    {
        float offset = Input::curr.wheel * ZoomSpeed;
        auto newPos = cameraPos + offset * normalize(cameraPos);
        if (dot(newPos, cameraPos) > 0)
        {
            float distance = length(newPos);
            if (distance > 0.1f && distance < 100.0f)
            {
                // zoom in or out
                cameraPos = newPos;
            }
        }
    }

    camera->transform->position = camera->targetPos + cameraPos;
}
