#pragma once

#include <list>

#include <Components/Behaviors/MonoBehavior.h>

#include <stdafx.h>

namespace walt
{
    enum class PrimitiveType;
    class GameObject;
}

namespace demo
{
    class BuoyancyController : public walt::MonoBehavior
    {
    public:
        BuoyancyController();
        ~BuoyancyController();

        void update() override;

        static glm::vec3 surfacePosition;
        static float fluidDensity;
    };
}
