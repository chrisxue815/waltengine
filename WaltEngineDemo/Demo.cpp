#include "Demo.h"

#include <iostream>
#include <WaltEngine.h>

#include <RigidBodyDemoScene.h>
#include <NarrowPhaseDemoScene.h>

using namespace walt;
using namespace demo;

demo::Demo::Demo(int* argc, char** argv)
{
    auto* engine = new WaltEngine(argc, argv);

    auto* scene = new NarrowPhaseDemoScene();
    //auto* scene = new RigidBodyDemoScene();
}

demo::Demo::~Demo()
{
}

void demo::Demo::run()
{
    engine->run();
}
