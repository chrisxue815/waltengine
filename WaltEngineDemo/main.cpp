#include <Demo.h>

#include <iostream>

using namespace demo;

int main(int argc, char** argv)
{
    Demo(&argc, argv).run();

    return 0;
}
