#pragma once

#include <glm/glm.hpp>
#include <Components/Behaviors/MonoBehavior.h>

#include <stdafx.h>

namespace demo
{
    class CameraController : public walt::MonoBehavior
    {
    public:
        CameraController();
        ~CameraController();

        virtual void update() override;

    protected:
        glm::vec2 prevPos;
    };
}
