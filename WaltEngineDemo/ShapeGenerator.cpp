#include "ShapeGenerator.h"

#include <ctime>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <Components/Renderers/Renderer.h>
#include <Components/Renderers/MeshRenderer.h>
#include <Components/Renderers/AABBRenderer.h>
#include <Components/Transform.h>
#include <Physics/Colliders/Collider.h>
#include <Color.h>
#include <GameObject.h>
#include <Input.h>
#include <Material.h>
#include <WaltEngine.h>
#include <PrimitiveGenerator.h>
#include <ShapeController.h>

using namespace demo;
using namespace walt;
using namespace glm;

ShapeGenerator::ShapeGenerator()
{
    srand((uint)time(NULL));
}

ShapeGenerator::~ShapeGenerator()
{
}

void ShapeGenerator::update()
{
    const float Cooldown = .5f;

    if (Input::getKey('1') && cooldown < WaltEngine::totalSeconds)
    {
        cooldown = WaltEngine::totalSeconds + Cooldown;

        int randTypeIndex = rand() % (int)PrimitiveType::NumTypes;
        auto randType = (PrimitiveType)randTypeIndex;

        auto shape = generate(randType);

        shapes.push_back(shape);
    }

    if (Input::getKey('2') && cooldown < WaltEngine::totalSeconds)
    {
        cooldown = WaltEngine::totalSeconds + Cooldown;

        auto shape = generate(PrimitiveType::Sphere);

        shapes.push_back(shape);
    }

    if (Input::getKey('3') && cooldown < WaltEngine::totalSeconds)
    {
        cooldown = WaltEngine::totalSeconds + Cooldown;

        auto shape = generate(PrimitiveType::Cube);

        shapes.push_back(shape);
    }

    if (Input::getKey('4') && cooldown < WaltEngine::totalSeconds)
    {
        cooldown = WaltEngine::totalSeconds + Cooldown;

        for (auto* shape : shapes)
        {
            ((MeshRenderer*)shape->renderer)->renderType = RenderType::WireFrame;
        }
    }

    if (Input::getKey('5') && cooldown < WaltEngine::totalSeconds)
    {
        cooldown = WaltEngine::totalSeconds + Cooldown;

        for (auto* shape : shapes)
        {
            ((MeshRenderer*)shape->renderer)->renderType = RenderType::Surface;
        }
    }

    if (Input::getKey('6') && cooldown < WaltEngine::totalSeconds)
    {
        cooldown = WaltEngine::totalSeconds + Cooldown;

        aabbRendererEnabled = !aabbRendererEnabled;

        for (auto* shape : shapes)
        {
            auto* aabbRenderer = (AABBRenderer*)shape->find("aabbRenderer");
            if (aabbRenderer == nullptr) continue;
            aabbRenderer->enabled = aabbRendererEnabled;
        }
    }

    if (Input::getKeyDown(' '))
    {
        WaltEngine::nextRunning = !WaltEngine::getRunning();
    }
}

GameObject* demo::ShapeGenerator::generate(PrimitiveType randType)
{
    auto* shape = PrimitiveGenerator::createPrimitive(randType);
    shape->transform->setParent(transform);
    shape->transform->position = vec3(0, 5, 0);
    float x = (float)fmod(rand(), 360);
    float y = (float)fmod(rand(), 360);
    float z = (float)fmod(rand(), 360);
    shape->transform->rotation = quat(vec3(x, y, z));
    shape->transform->scaling = vec3(0.5f);

    auto* collider = PrimitiveGenerator::createCollider(randType);
    shape->addComponent(collider);
    shape->collider->mass = 0.05f;

    auto* controller = new ShapeController();
    shape->addComponent(controller);

    auto* aabbRenderer = new AABBRenderer();
    aabbRenderer->name = "aabbRenderer";
    aabbRenderer->enabled = aabbRendererEnabled;
    shape->addComponent(aabbRenderer);

    auto* material = shape->renderer->getMaterial();
    material->setColor(vec3(0.8f));
    material->setLightDirection(vec3(1, -1, -1));
    material->setLightColor(vec4(1));
    material->setAmbientIntensity(0.55f);
    material->setDiffuseIntensity(0.6f);
    material->setSpecularIntensity(1);
    material->setSpecularPower(100);

    shapes.push_back(shape);

    return shape;
}
