#pragma once

#include <stdafx.h>

namespace walt
{
    class WaltEngine;
}

namespace demo
{
    class Demo
    {
    public:
        Demo(int* argc, char** argv);
        ~Demo();

        void run();

    protected:
        walt::WaltEngine* engine;
    };
}
