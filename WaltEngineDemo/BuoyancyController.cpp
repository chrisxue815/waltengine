#include "BuoyancyController.h"

#include <ctime>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/norm.hpp>

#include <Components/Renderers/Renderer.h>
#include <Components/Transform.h>
#include <Physics/Colliders/Collider.h>
#include <Material.h>
#include <Color.h>
#include <GameObject.h>
#include <Input.h>
#include <Material.h>
#include <WaltEngine.h>
#include <PrimitiveGenerator.h>

using namespace demo;
using namespace walt;
using namespace glm;

BuoyancyController::BuoyancyController()
{
    srand((unsigned int)time(NULL));
}

BuoyancyController::~BuoyancyController()
{
}

void BuoyancyController::update()
{
    float area = transform->scaling.x * transform->scaling.z;
    vec3 bottomPosition = transform->position() + transform->scaling.y / 2 * vec3(0, -1, 0);
    vec3 offset = surfacePosition - bottomPosition;

    if (offset.y > 0)
    {
        float volume = offset.y * area;
        float buoyancy = volume * fluidDensity * 9.8f;

        gameObject->collider->netForce += vec3(0, buoyancy, 0);

        vec3 currUp = transformDirection(Up, transform->rotation);

        float projectionLength = sqrt(currUp.x * currUp.x + currUp.z * currUp.z);

        vec3 projection = vec3(currUp.x, 0, currUp.z);
        vec3 axis = cross(projection, currUp);
        float angle = currUp.y > 0 ^ projectionLength > 0.5f ? buoyancy : -buoyancy;

        if (length2(axis) > 0)
        {
            vec3 currRight = transformDirection(Right, transform->rotation);
            float magnitude = dot(Right, currUp);

            gameObject->collider->netTorque += normalize(axis) * magnitude;
        }
    }

    float torquex = fmod(rand(), 100) / 100.f;
    float torquey = fmod(rand(), 100) / 100.f;
    float torquez = fmod(rand(), 100) / 100.f;
    //gameObject->collider->netTorque += vec3(torquex, torquey, torquez);
}

glm::vec3 demo::BuoyancyController::surfacePosition = vec3(0);
float demo::BuoyancyController::fluidDensity = 1;
