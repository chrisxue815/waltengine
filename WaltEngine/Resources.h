#pragma once

#include <stdafx.h>

namespace Assimp
{
    class Importer;
}

namespace walt
{
    enum class MaterialType
    {
        PhongMaterial,
        NumMaterialTypes
    };

    class GameObject;
    class Material;
    class Mesh;

    class Resources
    {
    public:
        Resources();
        ~Resources();

        static GameObject* loadGameObject(const char* path);
        static Mesh* loadMesh(const char* path);
        static Material* loadMaterial(MaterialType materialType);

        static Resources *instance;
    };
}
