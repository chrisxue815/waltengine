#pragma once

#include <Components/Behaviors/Behavior.h>

#include <stdafx.h>

namespace walt
{
    class MonoBehavior : public Behavior
    {
    public:
        MonoBehavior();
        ~MonoBehavior();
    };
}
