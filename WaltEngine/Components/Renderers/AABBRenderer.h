#pragma once

#include <memory>

#include <Components/Renderers/Renderer.h>

#include <stdafx.h>

namespace walt
{
    class Mesh;

    class AABBRenderer : public Renderer
    {
    public:
        AABBRenderer();
        ~AABBRenderer();

        virtual void draw() override;

        std::shared_ptr<Mesh> mesh;
    };
}
