#include "AABBRenderer.h"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

#include <Components/Camera.h>
#include <Components/MeshFilter.h>
#include <Components/Transform.h>
#include <Physics/AABB.h>
#include <Physics/Colliders/Collider.h>
#include <GameObject.h>
#include <Material.h>
#include <Mesh.h>
#include <Resources.h>
#include <Shader.h>

using namespace walt;
using namespace glm;
using namespace std;

AABBRenderer::AABBRenderer()
{
    materials.push_back(Resources::loadMaterial(MaterialType::PhongMaterial));

    mesh = shared_ptr<Mesh>(Resources::loadMesh("Assets/Models/Cube.dae"));
}

AABBRenderer::~AABBRenderer()
{
}

void AABBRenderer::draw()
{
    auto* material = getMaterial();
    auto* shader = material->shader;
    
    auto worldMatrix = translate(transform->position()) * scale(gameObject->collider->aabb->scale());
    auto wvp = Camera::current->vpMatrix * worldMatrix;

    material->setWVP(wvp);
    material->setWorldMatrix(worldMatrix);
    material->setCameraPos(Camera::current->transform->position());

    mesh->bind();

    glDrawElements(
        GL_LINE_STRIP,
        mesh->numTriangles * 3,
        GL_UNSIGNED_INT,
        0);
}
