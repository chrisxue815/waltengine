#include "AABB.h"

#include <Components/Transform.h>
#include <Physics/Colliders/Collider.h>
#include <GameObject.h>

using namespace walt;
using namespace glm;

AABB::AABB(const vec3& scale)
    : halfScale(scale / 2.f)
{
}

AABB::~AABB()
{
}

bool AABB::FindCollision(Collider& colliderA, Collider& colliderB)
{
    auto posA = colliderA.transform->position();
    auto posB = colliderB.transform->position();
    auto a = colliderA.aabb;
    auto b = colliderB.aabb;

    if (posA.x < posB.x)
    {
        if (posA.x + a->halfWidth() < posB.x - b->halfWidth()) return false;
    }
    else
    {
        if (posB.x + b->halfWidth() < posA.x - a->halfWidth()) return false;
    }

    if (posA.y < posB.y)
    {
        if (posA.y + a->halfHeight() < posB.y - b->halfHeight()) return false;
    }
    else
    {
        if (posB.y + b->halfHeight() < posA.y - a->halfHeight()) return false;
    }

    if (posA.z < posB.z)
    {
        if (posA.z + a->halfDepth() < posB.z - b->halfDepth()) return false;
    }
    else
    {
        if (posB.z + b->halfDepth() < posA.z - a->halfDepth()) return false;
    }

    return true;
}
