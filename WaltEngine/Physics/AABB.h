#pragma once

#include <list>

#include <stdafx.h>

namespace walt
{
    class Collider;

    class AABB
    {
    public:
        AABB(const glm::vec3& scale);
        ~AABB();

        static bool FindCollision(Collider& a, Collider& b);

        glm::vec3 halfScale;
        
        glm::vec3 scale() { return halfScale * 2.f; }
        void setScale(const glm::vec3& value) { halfScale = value / 2.f; }

        float halfWidth() { return halfScale.x; }
        float halfHeight() { return halfScale.y; }
        float halfDepth() { return halfScale.z; }
    };
}
