#include "BoxCollider.h"

#include <glm/gtx/transform.hpp>

#include <Components/Transform.h>
#include <Physics/AABB.h>
#include <GameObject.h>

using namespace walt;
using namespace glm;
using namespace std;

BoxCollider::BoxCollider()
    : Collider()
{
}

BoxCollider::~BoxCollider()
{
}

vec3 BoxCollider::findSupport(vec3& direction)
{
    vec3 localDirection = transform->toLocalDirection(direction);

    auto x = normalize(localDirection.x) * 0.5f;
    auto y = normalize(localDirection.y) * 0.5f;
    auto z = normalize(localDirection.z) * 0.5f;
    auto localSupport = vec3(x, y, z);
    return transform->toWorldPosition(localSupport);
}

void BoxCollider::updateInetiaTensor()
{
    if (inverseMass != 0)
    {
        auto wSqr = transform->scaling.x * transform->scaling.x;
        auto hSqr = transform->scaling.y * transform->scaling.y;
        auto dSqr = transform->scaling.z * transform->scaling.z;
        auto c = mass / 12;

        mat3 inertiaTensor(
            c * (hSqr + dSqr), 0, 0,
            0, c * (wSqr + dSqr), 0,
            0, 0, c * (wSqr * hSqr));

        setInertiaTensor(inertiaTensor);
    }
    else
    {
        inverseInertiaTensor = mat3(0);
    }
}

void walt::BoxCollider::beforeAttach(GameObject* gameObject)
{
    Collider::beforeAttach(gameObject);

    float diameter = length(gameObject->transform->scaling);
    aabb = shared_ptr<AABB>(new AABB(vec3(diameter)));
}
