#include "SphereCollider.h"

#include <glm/gtx/transform.hpp>

#include <Components/Transform.h>
#include <Physics/AABB.h>
#include <GameObject.h>

using namespace walt;
using namespace glm;
using namespace std;

SphereCollider::SphereCollider()
    : Collider()
{
}

SphereCollider::~SphereCollider()
{
}

vec3 SphereCollider::findSupport(vec3& direction)
{
    vec3 localDirection = transform->toLocalDirection(direction);
    localDirection *= 0.5f / length(localDirection);

    return transform->toWorldPosition(localDirection);
}

void SphereCollider::updateInetiaTensor()
{
    if (inverseMass != 0)
    {
        auto wSqr = transform->scaling.x * transform->scaling.x;
        auto hSqr = transform->scaling.y * transform->scaling.y;
        auto dSqr = transform->scaling.z * transform->scaling.z;
        auto c = mass / 5;

        mat3 inertiaTensor(
            c * (hSqr + dSqr), 0, 0,
            0, c * (wSqr + dSqr), 0,
            0, 0, c * (wSqr * hSqr));

        setInertiaTensor(inertiaTensor);
    }
    else
    {
        inverseInertiaTensor = mat3(0);
    }
}

void walt::SphereCollider::beforeAttach(GameObject* gameObject)
{
    Collider::beforeAttach(gameObject);

    float diameter = max(gameObject->transform->scaling);
    aabb = shared_ptr<AABB>(new AABB(vec3(diameter)));
}
